where_on_earth_IDs = {'Albania': 963291, 'Algeria': 23424740, 'Argentina': 23424747, 'Armenia': 2343932, 'Austria': 23424750,
'Australia': 23424748, 'Bahamas': 2450022, 'Bahrain': 23424753, 'Belarus': 23424765, 'Belgium': 23424757, 
'Brazil': 23424768, 'Bulgaria': 963291, 'Canada': 23424775, 'Chile': 349861, 'China': 1236594, 'Colombia': 23424787,
'Costa Rica': 23424924, 'Croatia': 551801, 'Cuba': 2450022, 'Cyprus': 2323778, 'Czech Republic': 526363, 'Denmark': 23424796,
'Dominica': 23424800, 'Dominican Republic': 23424800, 'Ecuador': 375733, 'Egypt': 23424802, 'Estonia': 23424874, 'Ethiopia': 23424863,
'Finland': 2123260, 'France': 23424819, 'Guatamala': 23424834, 'Georgia': 2357024, 'Germany': 23424829, 'Greece': 23424833,
'Honduras': 23424834, 'Hong Kong': 1236690, 'Hungary': 551801, 'Iceland': 21125, 'India': 2282863, 'Indonesia': 1046138, 
'Iran': 23424870, 'Iraq': 23424870, 'Ireland': 23424803, 'Israel': 1968222, 'Italy': 23424853, 'Jamaica': 23424800, 
'Japan': 1116753, 'Jordan': 23424860, 'Kuwait': 23424870, 'Latvia': 23424874, 'Lebanon': 23424873, 'Libya': 1522006, 'Lithuania': 23424874, 'Luxembourg': 667931, 'Madagascar': 1528335, 'Malaysia': 1154679, 
'Maldives': 2295420, 'Mauritius': 1528335, 'Mexico': 116545, 'Moldova': 929398, 'Mongolia': 2121040, 'Morocco': 766356, 
'Netherlands': 23424909, 'New Zealand': 23424916, 'Nigeria': 23424908, 'Norway': 862592, 'Oman': 23424898, 'Panama': 23424924, 
'Paraguay': 455822, 'Peru': 23424919, 'Philippines': 1198785, 'Poland': 23424923, 'Portugal': 23424925, 'Qatar': 23424930, 
'Romania': 924943, 'Russia': 23424936, 'Rwanda': 1528488, 'Saudi Arabia': 1937801, 'Serbia': 963291, 'Slovakia': 502075, 
'Slovenia': 23424750, 'South Africa': 23424942, 'South Korea': 23424868, 'Sudan': 1939873, 'Spain': 766273, 'Sri Lanka': 2295424, 
'Sweden': 906057, 'Singapore': 23424948, 'Switzerland': 23424957, 'Taiwan': 2345896, 'Thailand': 1225448, 'Tanzania': 1528335, 
'Tunisia': 719846, 'Turkey': 23424969, 'Turkmenistan': 23424922, 'Ukraine': 924938, 'United Arab Emirates': 1940330, 
'United Kingdom': 23424975, 'United States': 2391279, 'Uruguay': 468739, 'Venezuela': 23424982, 'Vietnam': 23424984, 
'Zimbabwe': 1586638}

English = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
Numbers = "0123456789"