import React, { memo } from "react";
import {
  ComposableMap,
  Geographies,
  Geography, 
  Sphere
} from "react-simple-maps";



const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const MapChart = ({ setTooltipContent, data }) => {
  return (
    <>
      <ComposableMap data-tip="" projectionConfig={{ scale: 150 }}>
      <Sphere stroke="#61d4b3" strokeWidth={2} />
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map(geo => (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onMouseEnter={() => {
                    const { NAME, ISO_A2 } = geo.properties;
                    var tweet = ""
                    if (ISO_A2 in data){
                      tweet = data[ISO_A2]
                    }
                    else{
                      tweet = "None"
                    }
                    setTooltipContent(`${NAME} — ${tweet}`);
                  }}
                  onMouseLeave={() => {
                    setTooltipContent("");
                  }}
                  
                  style={{
                    default: {
                      fill: "#61d4b3",
                      outline: "none"
                    },
                    hover: {
                      fill: "#ffc106",
                      outline: "none"
                    },
                    pressed: {
                      fill: "#d46182",
                      outline: "none"
                    }
                  }}
                />
              ))
            }
          </Geographies>
  
      </ComposableMap>
    </>
  );
};

export default memo(MapChart);
