import React from "react";
import ReactDOM from "react-dom";
import {act, renderIntoDocument, findRenderedDOMComponentWithClass} from 'react-dom/test-utils';
import { render } from "@testing-library/react";
import { configure, shallow, mount, dive} from "enzyme"; //React testing library
import { expect } from "chai";
import App from "./App";
import Adapter from "enzyme-adapter-react-16";
import { MemoryRouter, Route } from "react-router-dom";
import Navbar from "./components/NavBar.jsx";
import Pagination from "./components/Pagination.jsx";
import Home from "./Pages/Home/Home.jsx";
import About from "./Pages/About/About.jsx";
import Countries from "./Pages/Countries/Countries.jsx";
import News from "./Pages/News/News.jsx";
import Tweets from "./Pages/Tweets/Tweets.jsx";
import CountryProfile from "./Pages/Countries/CountryProfile.jsx";
import NewsProfile from "./Pages/News/NewsProfile.jsx";
import TweetProfile from "./Pages/Tweets/TweetProfile.jsx";
import Search from "./Pages/Home/Search.jsx";

configure({ adapter: new Adapter() });

let rootContainer;

beforeEach(() => {
	rootContainer = document.createElement("div");
	document.body.appendChild(rootContainer);
});

afterEach(() => {
	document.body.removeChild(rootContainer);
	rootContainer = null;
});

describe("#app", () => {
	it("renders app without crashing", () => {
		act(() => {
			ReactDOM.render(<App />, rootContainer);
		});
	});
});

 describe("#countries", () => {
	it("renders Countries without Crashing", () => {
		act(() => {
			ReactDOM.render(
				<MemoryRouter initialEntries={["/countries"]}>
					<Route path="/countries" component = {Countries}>
					</Route>
				</MemoryRouter>,
				rootContainer
			);
    });
  });
  
	 it("renders Countries' title", () => {
    const container = shallow(<Countries />);
    expect(container).to.not.be.undefined;
    expect(container.find("h1")).to.not.be.undefined;
    setTimeout(function () {
      timedOut = true;
  }, 500);
	});

	it("Countries has all its functions", function () {
		let a = new Countries();
		expect(!(typeof a.state === "undefined"));
		expect(typeof a.handleInputChange === "function");
		expect(typeof a.createCountriesCards === "function");
		expect(typeof a.componentDidMount === "function");
		expect(typeof a.render === "function");
		expect(typeof a.highlight === "function");
		expect(typeof a.onPageChanged === "function");
  });
  
  it("Countries has fetched some countries from api",()=>{
    const wrapper = mount(<Countries/>);
    expect(wrapper.state().countries).to.be.null;
    expect(wrapper.state().filteredCountries).to.have.length(0);
  });

	it("CountryProfile defined", () => {
		//“shallowly” render a component. Just that component
		const component = shallow(<CountryProfile />);
		expect(component).to.not.be.undefined;
  });

});

describe("#tweets", () => {
	it("renders tweets without Crashing", () => {
		const div = document.createElement("div");
		ReactDOM.render(
			<MemoryRouter initialEntries={["/tweets"]}>
				<Route path="/tweets">
					<TweetProfile />
				</Route>
			</MemoryRouter>,
			div
		);
		ReactDOM.unmountComponentAtNode(div);
	});

	it("Tweets has all functions", function () {
		let a = new Tweets();
		expect(!(typeof a.state === "undefined"));
		expect(typeof a.handleInputChange === "function");
		expect(typeof a.createTweetsCards === "function");
		expect(typeof a.componentDidMount === "function");
		expect(typeof a.render === "function");
		expect(typeof a.highlight === "function");
		expect(typeof a.handleFilter === "function");
		expect(typeof a.onPageChanged === "function");
		expect(typeof a.getCountry === "function");
  });

	it("TweetProfile defined", () => {
		const component = shallow(<TweetProfile />);
		expect(component).to.not.be.undefined;
	});
});

describe("#news", () => {
	it("renders News without Crashing", () => {
		const div = document.createElement("div");
		ReactDOM.render(
			<MemoryRouter initialEntries={["/news"]}>
				<Route path="/news">
					<NewsProfile />
				</Route>
			</MemoryRouter>,
			div
		);
		ReactDOM.unmountComponentAtNode(div);
	});

	it("News has all functions", function () {
		let a = new News();
		expect(!(typeof a.state === "undefined"));
		expect(typeof a.handleInputChange === "function");
		expect(typeof a.createNewsCards === "function");
		expect(typeof a.componentDidMount === "function");
		expect(typeof a.render === "function");
		expect(typeof a.highlight === "function");
		expect(typeof a.onPageChanged === "function");
  });

	it("NewsProfile defined", () => {
		const component = shallow(<NewsProfile />);
		expect(component).to.not.be.undefined;
  });
  
});

describe("#about", () => {
	it("About defined", () => {
		const component = shallow(<About />);
		expect(component).to.not.be.undefined;
	});

	it("About tests", function () {
		let a = new About();
		expect(!(typeof a.state === "undefined"));
		expect(typeof a.getCommitCount === "function");
		expect(typeof a.getIssueCount === "function");
		expect(typeof a.componentDidMount === "function");
		expect(typeof a.render === "function");
		expect(typeof a.getTotaltests === "function");
		expect(typeof a.getTotalcommits === "function");
	});
});

describe("#home", () => {
	it("Home defined", () => {
		const component = shallow(<Home />);
		expect(component).to.not.be.undefined;
	});

	it("Home has all functions", function () {
		let v = new Home();
		expect(!(typeof v.state === "undefined"));
		expect(typeof v.renderSearch === "function");
		expect(typeof v.renderURL === "function");
		expect(typeof v.render === "function");
  });
  it("Should have a search bar", ()=>{
    const wrapper = shallow(<Home />);
    expect(wrapper.find('input')).to.have.length(1);
  });
});

//components are rendering, receiving props and state, and updating properly
describe("#navBar", () => {
	it("Navbar defined", () => {
		const component = shallow(<Navbar />);
		expect(component).to.not.be.undefined;
	});
});