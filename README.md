# The Buzzz

### Purpose 
To inform users of what’s going on around them by displaying popular 
topics talked about on social media, the news, and country.

### Website
www.thebuzzz.me

## Group Members
| Name  | EID | GitLab Username |
| ------------- | ------------- | ------------- |
| Simrat Chandi  | sc54668  | simc97 |
| SeongBeom Ko  | sk32262  | ksb9257  |
| Asu Dhakal  | ad42324  | asutoshdhakal  |
| Rudra Garg | rg44778 | rudragarg  |
| Zelma Garza | zag376  | zelmag  |
| Shivani Revuru | slr3597 | Shiviiii23  |

## Completion Times (Phase IV)
| Total Estimated Completion Time | Total Actual Completion Time |
|  ------------- | ------------- |
|  50   hours | 61  hours |

| Name | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| Simrat Chandi |  10  hours | 15 hours |
| SeongBeom Ko | 7 hours | 7 hours |
| Asu Dhakal (Project Lead) |    5 hours |   6 hours |
| Rudra Garg | 10  hours | 15  hours |
| Shivani Revuru | 5 hours | 5 hours |
| Zelma Garza  |  9 hours |   13 hours |

## Completion Times (Phase III)
| Total Estimated Completion Time | Total Actual Completion Time |
|  ------------- | ------------- |
|    50 hours |   76 hours |

| Name | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| Simrat Chandi |   10 hours | 15 hours |
| SeongBeom Ko | 10 hours | 15 hours |
| Asu Dhakal |   10 hours |  14 hours |
| Rudra Garg (Project Lead) |  10  hours | 10  hours |
| Shivani Revuru | 10 hours | 7 hours |
| Zelma Garza  | 10 hours |  15 hours |

## Completion Times (Phase II)
| Total Estimated Completion Time | Total Actual Completion Time |
|  ------------- | ------------- |
|   105 hours |  147 hours |

| Name | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| Simrat Chandi |   20 hours | 25 hours |
| SeongBeom Ko | 20 hours | 30 hours |
| Asu Dhakal | 15  hours | 16 hours |
| Rudra Garg |  15  hours | 25  hours |
| Shivani Revuru | 15 hours | 27 hours |
| Zelma Garza  (Project Lead) | 20 hours | 24 hours |

## Completion Times (Phase I)
| Total Estimated Completion Time | Total Actual Completion Time |
|  ------------- | ------------- |
|   30 hours |  74 hours |

| Name | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| Simrat Chandi (Project Lead)|   10 hours | 15 hours |
| SeongBeom Ko | 5 hours | 7 hours |
| Asu Dhakal |   8 hours |  10 hours |
| Rudra Garg |   10 hours |  12 hours |
| Shivani Revuru |   8 hours |  12 hours |
| Zelma Garza |  15 hours |  18 hours |

## Git SHA

Phase I - 587b59622f587dd03314d5adb7e6095127772e7f
Phase II - 604c308350207e29e1e8d6cd5c58fa2fa566cba5
Phase III - 20e93f0302fdd4040886c736e2cc2a3f1fd0d66b
Phase IV - 95050386ed8864b61d70d8b0239fc7359f105fc9

## GitLab Pipelines

[click here](https://gitlab.com/simc97/the-buzzz/pipelines): https://gitlab.com/simc97/the-buzzz/pipelines


Comment: 
